<!DOCTYPE html>
@php $lang = App::getLocale() @endphp
<html lang="{{$lang}}">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">

        <link rel="stylesheet" href="/css/locomotive-scroll.css">
        <link rel="stylesheet" href="/fonts/stylesheet.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="icon" type="image/x-icon" href="/img/logo.svg">

        <title>Ip net</title>
    </head>
    <body>
        <header>
            <div class="container">
                <a href="/"><img class="logo" src="/img/logo.svg" alt=""></a>
                <div class="menu">
                    <div class="menu-toggle __top">
                        <span>меню</span>
                        <div class="circle"></div>
                    </div>
                    <div class="__inner">
                        <img class="menu-bg" src="/img/menu-bg.svg" alt="">
                        <img class="close-cross menu-toggle" src="/img/close-cross.svg" alt="">
                        <nav>
                            <ul>
                                <li><a href="/about-us">О компании</a></li>
                                <li><a href="#services" class="anchor">Наши услуги</a></li>
                                <li><a href="#partners" class="anchor">Партнеры</a></li>
                                <li><a href="#footer" class="anchor">Контакты</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <div id="content" class="smooth-scroll" data-scroll-container>
            @yield('content')
            <footer id="footer">
                <img class="footer-bg" src="/img/footer-bg.svg" alt="">
                <div class="container">
                    <div class="row">
                        <div>
                            <a href="#"><img src="/img/logo.svg" alt=""></a>
                            <div class="socials">
                                <a href="#" target="_blank">
                                    <img src="/img/in-icon.svg" alt="">
                                </a>
                                <a href="#" target="_blank">
                                    <img src="/img/instagram-icon.svg" alt="">
                                </a>
                                <a href="#" target="_blank">
                                    <img src="/img/facebook-icon.svg" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="contact-info">
                            <h2>Контакты</h2>
                            <p>Кыргызская Республика, <br>г. Бишкек, ул. Раззакова, 55</p>
                            <a class="tel" href="tel: +996312963999">+ 996 312 963 999</a>
                            <a class="email" href="mailto: office@ipnet.kg">office@ipnet.kg</a>
                        </div>
                    </div>
                    <div class="row __bottom">
                        <div>All rights reserved © 2023  IPNET</div>
                        <div>Powered by <a href="https://brainteam.kz/ru" target="_blank">BrainTeam.kz</a> © 2023</div>
                    </div>
                </div>
            </footer>
        </div>
        <script src="/js/CustomEase.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/ScrollTrigger.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/ScrollToPlugin.min.js"></script>
        <script src="/js/locomotive-scroll.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"
                integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
        <script src="/js/jquery.maskedinput.min.js"></script>
        <script src="/js/script.js"></script>
    </body>
</html>
