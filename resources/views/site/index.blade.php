@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    <section id="top-section">
        <div class="container">
            <img class="top-section__bg" src="/img/top-section__bg.svg" alt="">
            <h1>
                Широкополосный доступ в Интернет
                с компанией ОсОО «IP NET»
            </h1>
            <a href="/about-us" class="btn">
                подробнее
                <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                </svg>
            </a>
        </div>
    </section>
    <section id="icons-info">
        <div class="container">
            <div class="row">
                <div>
                    <div class="img-outer">
                        <img class="item-bg" src="/img/item-bg.svg" alt="">
                        <img data-scroll data-scroll-speed="1" class="icon-item" src="/img/icon-1.svg" alt="">
                    </div>
                    <p>Работаем на оборудовании Cisco, также имеем в наличии Google Cache</p>
                </div>
                <div>
                    <div class="img-outer">
                        <img class="item-bg" src="/img/item-bg.svg" alt="">
                        <img  data-scroll data-scroll-speed="1" class="icon-item" src="/img/icon-2.svg" alt="">
                    </div>
                    <p>Доля участия на рынке Кыргызской республики составляет 20-25%</p>
                </div>
                <div>
                    <div class="img-outer">
                        <img class="item-bg" src="/img/item-bg.svg" alt="">
                        <img  data-scroll data-scroll-speed="1" class="icon-item" src="/img/icon-3.svg" alt="">
                    </div>
                    <p>Используем самые современные технологии и новейшее оборудование</p>
                </div>
            </div>
        </div>
    </section>
    <section id="services">
        <img class="services-bg" src="/img/services-bg.svg" alt="">
        <div class="container">
            <h2 class="animation_fade-top" data-scroll>Наши услуги</h2>
            <div>
                <h3>Услуги для операторов</h3>
                <p>ОсОО «IP NET» предоставляет следующие виды услуг для операторов:</p>
                <div class="row card-items">
                    <div class="col-33">
                        <div class="card">
                            <h3>IP Транзит</h3>
                            <ul>
                                <li>Подключение к Интернету до 1 Тбит/с.</li>
                                <li>Доступ в крупнейшие точки обмена.</li>
                                <li>Наличие резервированных каналов.</li>
                            </ul>
                            <a href="/service/ip-tranzit" class="btn">
                                подробнее
                                <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="col-33">
                        <div class="card">
                            <h3>IPLC</h3>
                            <ul>
                                <li>IPLC — единая сеть между филиалами.</li>
                                <li>Наивысшее качество соединения.</li>
                                <li>Организация голосовой и видеосвязи.</li>
                            </ul>
                            <a href="/service/iplc" class="btn">
                                подробнее
                                <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="col-33">
                        <div class="card">
                            <h3>Ethernet (IEPL)</h3>
                            <ul>
                                <li>Быстрая передача внутреннего трафика.</li>
                                <li>Прямая связь от 1 Мбит/с до 1 Гбит/с.</li>
                            </ul>
                            <a href="/service/ethernet" class="btn">
                                подробнее
                                <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                                </svg>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="business-service">
                <h3>Услуги для бизнеса</h3>
                <p>ОсОО «IP NET» предоставляет следующие виды услуг для Вашего бизнеса:</p>
                <div class="row card-items">
                    <div class="col-33">
                        <div class="card">
                            <h3>Глобальный Интернет</h3>
                            <ul>
                                <li>Услуга локального доступа в Интернет.</li>
                                <li>Локальная петля (DSL, оптоволокно).</li>
                                <li>Служба поддержки 24/7/365.</li>
                            </ul>
                            <a href="/service/global-internet" class="btn">
                                подробнее
                                <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="col-33">
                        <div class="card">
                            <h3>Проводной интернет</h3>
                            <ul>
                                <li>Подключение к Интернету до 1 Тбит/с.</li>
                                <li>Быстрый доступ к Интернет ресурсам.</li>
                                <li>Доступ в Интернет целый год 24/7/365.</li>
                            </ul>
                            <a href="/service/cabell-internet" class="btn">
                                подробнее
                                <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="key-factors">
        <div class="container">
            <div class="title-with-arrow">
                <h2 class="animation_fade-top" data-scroll>
                    Ключевые факторы<br>
                    успешного развития
                </h2>
                <div class="long-arrow"></div>
            </div>
            <div class="row card-items-2">
                <div class="col-33">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>1</span></div>
                        <p>Максимальное использование ВОЛС магистраль РК (с. Кордай) – КР Бишкек – КР Юг;</p>
                    </div>
                </div>
                <div class="col-33">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>2</span></div>
                        <p>Максимальное использование ВОЛС магистраль с.Чалдовар – граница КР и РК;</p>
                    </div>
                </div>
                <div class="col-33">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>3</span></div>
                        <p>Предоставление конкурентоспособной стоимости услуг для местных и международных операторов.;</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="partners">
        <div class="container">
            <h2 class="animation_fade-top" data-scroll>Партнеры</h2>
            <div class="swiper" id="swiper_partners">
                <div class="swiper-wrapper">
                    <div class="swiper-slide"><img src="/img/partner-1.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-2.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-3.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-4.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-5.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-6.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-7.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-8.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-9.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-10.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-11.svg" alt=""></div>
                    <div class="swiper-slide"><img src="/img/partner-12.svg" alt=""></div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </section>
    <section id="consultation">
        <div class="container">
            <h2 class="animation_fade-top" data-scroll>Для консультации <br>вы можете заказать <br>звонок</h2>
            <form action="/application/send" class="__default">
                <label>
                    <input type="text" placeholder="Компания" name="company">
                </label>
                <label>
                    <input type="text" placeholder="Контактное лицо" name="contact-person" required>
                </label>
                <label>
                    <input type="text" placeholder="Номер телефона" name="phone" class="input-phone" required>
                </label>
                <button type="submit" class="btn">
                    заказать звонок
                    <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                    </svg>
                </button>
            </form>
        </div>
    </section>
    <section id="map">
        <div class="container map-container">
            <img class="map" src="/img/map.svg" alt="">
        </div>
    </section>
@stop
