@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    @include('partials.breadcrumbs')
    <section id="about-company__top">
        <div class="container">
            <div class="row row-text-img">
                <div class="info">
                    <h1>О компании</h1>
                    <h3>Год образования — 2011 год.</h3>
                    <p>
                        ОсОО «IP NET» — магистральный провайдер, который имеет магистральные стыки с Российской Федерацией и Республикой Казахстан.<br>
                        Мы предоставляем качественные и конкурентоспособные интернет услуги по всему Кыргызстану.
                        <br><br>
                        Вид деятельности — предоставление ШПД операторам в КР г. Бишкек.
                    </p>
                    <h3>Партнеры</h3>
                    <p>Основными партнерами ОсОО «IP NET» по предоставлению в аренду каналов связи КР, являются крупные и средние интернет компании, которые обслуживают население, государственные учреждения и коммерческие организации Республики Кыргызстан, а так же нашими партнерами являются компании, которые находятся в Республике Казахстан, Федеративной Республики Германия и Российской Федерации..</p>
                </div>
                <div>
                    <div class="img-overflow" data-scroll data-scroll-speed="4">
                        <img src="/img/about-us-1.jpg" alt="" data-scroll="" data-scroll-speed="-2">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="key-factors">
        <div class="container">
            <div class="title-with-arrow">
                <h2 class="animation_fade-top" data-scroll>
                    Ключевые факторы<br>
                    успешного развития
                </h2>
                <div class="long-arrow"></div>
            </div>
            <div class="row card-items-2">
                <div class="col-33">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>1</span></div>
                        <p>Максимальное использование ВОЛС магистраль РК (с. Кордай) – КР Бишкек – КР Юг;</p>
                    </div>
                </div>
                <div class="col-33">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>2</span></div>
                        <p>Максимальное использование ВОЛС магистраль с.Чалдовар – граница КР и РК;</p>
                    </div>
                </div>
                <div class="col-33">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>3</span></div>
                        <p>Предоставление конкурентоспособной стоимости услуг для местных и международных операторов.;</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="arrow-items">
        <div class="container">
            <div class="item">
                <div class="title-with-arrow" data-scroll>
                    <h2 class="animation_fade-top" data-scroll>Планы</h2>
                    <div class="long-arrow"></div>
                </div>
                <div class="gray-bg">
                    <img class="arrow-item_gray-bg" src="/img/arrow-item_gray-bg.svg" alt="">
                    <ul class="type-2" data-scroll data-scroll-speed="1">
                        <li>Приобретение дополнительных ВОЛС и строительство собственных дополнительных каналов связи;</li>
                        <li>Полный охват всей территории КР магистральными сетями;</li>
                        <li>Приобретение и инсталляция необходимого оборудования;</li>
                        <li>Предоставление в аренду канала связи и продажа дополнительного объема ШПД.</li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <div class="title-with-arrow" data-scroll>
                    <h2 class="animation_fade-top" data-scroll>Позиционирование</h2>
                    <div class="long-arrow"></div>
                </div>
                <div class="gray-bg">
                    <img class="arrow-item_gray-bg" src="/img/arrow-item_gray-bg.svg" alt="">
                    <ul class="type-2" data-scroll data-scroll-speed="1">
                        <li>Предоставление операторам новых возможностей;</li>
                        <li>Высокое качество предоставляемых услуг;</li>
                        <li>Удешевление стоимости доступа в Интернет.</li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <div class="title-with-arrow" data-scroll>
                    <h2 class="animation_fade-top" data-scroll>Особенности</h2>
                    <div class="long-arrow"></div>
                </div>
                <div class="gray-bg">
                    <img class="arrow-item_gray-bg" src="/img/arrow-item_gray-bg.svg" alt="">
                    <ul class="type-2" data-scroll data-scroll-speed="1">
                        <li>Приобретение дополнительных ВОЛС и строительство собственных дополнительных каналов связи;</li>
                        <li>Полный охват всей территории КР магистральными сетями;</li>
                        <li>Приобретение и инсталляция необходимого оборудования;</li>
                        <li>Предоставление в аренду канала связи и продажа дополнительного объема ШПД.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="about-company__bottom">
        <div class="container">
            <div class="row row-text-img">
                <div class="info">
                    <h2 class="animation_fade-top" data-scroll>Задачи</h2>
                    <p>
                        Сегодня операторы связи КР, испытывают дефицит емкости каналов связи для расширения портфеля своих услуг в Южном направлении. Принимая во внимание постепенное насыщение рынка услуг связи на Севере Кыргызстана, перспективным и выгодным с точки зрения инвестиций для операторов становится рынок услуг южных областей КР;
                        <br><br>
                        В настоящий момент компания ОсОО «IP NET» имеет тесные связи и партнеров в Республике Таджикистан, с которыми создается совместная платформа и проекты для расширения нашего сотрудничества;
                        <br><br>
                        Также в ближайшей перспективе нами планируется выход на рынок Республики Узбекистан, в связи с потеплением двухсторонних отношений между нашими странами и стремительному развитию межгосударственных экономических связей.
                    </p>
                </div>
                <div>
                    <div class="img-overflow" data-scroll data-scroll-speed="4">
                        <img src="/img/about-us-2.jpg" alt="" data-scroll="" data-scroll-speed="-2">
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
