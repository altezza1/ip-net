@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    @include('partials.breadcrumbs')
    <section id="service-page">
        <div class="container">
            <h1>IPLC</h1>
            <div class="__top">
                <p>
                    IPLC (INTERNATIONAL PRIVATE LEASED CIRCUIT) — прозрачный высокоскоростной, выделенный и полностью цифровой канал.
                    <br><br>
                    С помощью IPLC возможна организация международной частной линии для соединения офиса клиента, находящегося на территории одной страны, с офисом расположенным в другой стране, посредством прозрачного высокоскоростного выделенного и полностью цифрового канала.
                </p>
                <a href="/#consultation" class="btn to-callback-modal">
                    заказать звонок
                    <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                    </svg>
                </a>
            </div>
            <div class="row card-items-2">
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>1</span></div>
                        <p>C помощью IPLC можно создать единую сеть между филиалами одной компании на разных географических территориях.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>2</span></div>
                        <p>Обеспечивает наивысшее качество сетевых соединений для критически важных приложений.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>3</span></div>
                        <p>Передача различного вида трафика по прозрачным, выделенным каналам связи.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>4</span></div>
                        <p>Обеспечивает выделенное, безопасное соединение между различными городами, странами и континентами.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
