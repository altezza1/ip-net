@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    @include('partials.breadcrumbs')
    <section id="service-page">
        <div class="container">
            <h1>IP Транзит</h1>
            <div class="__top">
                <p>
                    IP Транзит — проверенные решения для высокой скорости и надежности.
                    <br><br>
                    IP Транзит стал востребованным решением для современного бизнеса, обеспечивая стабильный и высокоскоростной доступ к Интернету на базе нашей магистральной MPLS-сети. Проверенные решения и тщательный контроль соединения позволяют гарантировать широкие возможности для операторов.
                </p>
                <a href="/#consultation" class="btn to-callback-modal">
                    заказать звонок
                    <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                    </svg>
                </a>
            </div>
            <div class="row card-items-2">
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>1</span></div>
                        <p>Подключение к Интернету до 1 Тбит/с.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>2</span></div>
                        <p>Прямой доступ в крупнейшие точки обмена трафиком.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>3</span></div>
                        <p>Наличие резервированных международных и междугородних каналов.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>4</span></div>
                        <p>Круглосуточная 24/7/365 техническая поддержка</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>5</span></div>
                        <p>Гибкие тарифные планы.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
