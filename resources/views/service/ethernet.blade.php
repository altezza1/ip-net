@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    @include('partials.breadcrumbs')
    <section id="service-page">
        <div class="container">
            <h1>Ethernet (IEPL)</h1>
            <div class="__top">
                <p>
                    IEPL — (International Ethernet Private Line) защищенные, масштабируемые EoSDH Layer 1 point-to-point соединения.
                    <br><br>
                    Установите связь с офисами по всему миру с возможностью подключения, которая удовлетворяет все ваши потребности в передаче информации и данных от голосовой связи, данных и видеосвязи. Управление вашим бизнесом в нескольких точках никогда не было проще.
                    Услуга IEPL обеспечивает масштабируемое и безопасное международное соединение операторского класса (от 2 Мбит/с до 1 Гбит/с) для предприятий, которым необходимо соединить свои офисы по всему региону.
                </p>
                <a href="/#consultation" class="btn to-callback-modal">
                    заказать звонок
                    <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                    </svg>
                </a>
            </div>
            <div class="row card-items-2">
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>1</span></div>
                        <p>Быстрая и безопасная передача внутреннего трафика по выделенным каналам в любую точку мира.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>2</span></div>
                        <p>Прямая связь с несколькими крупными международными телекоммуникационными рынками на скоростях от 1 Мбит/с до 1 Гбит/с.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>3</span></div>
                        <p>Возможность резервировать выделенные каналы на уровне Layer 1.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
