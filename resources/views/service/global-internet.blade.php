@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    @include('partials.breadcrumbs')
    <section id="service-page">
        <div class="container">
            <h1>Глобальный интернет</h1>
            <div class="__top">
                <p>
                    Выделенный и высокоскоростной доступ в Интернет.
                    <br><br>
                    Global Internet — это услуга, которая позволяет вам предоставлять вашим международным офисам интернет-услуги бизнес-класса под единым управлением и по единому счету.
                    <br><br>
                    Вместо того, чтобы управлять рядом поставщиков с различными уровнями обслуживания, службами поддержки, языками и счетами, Global Internet предоставляет согласованный и надежный сервис, подходящий для ваших бизнес-приложений, создания корпоративных VPN.
                </p>
                <a href="/#consultation" class="btn to-callback-modal">
                    заказать звонок
                    <svg width="46" height="10" viewBox="0 0 46 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M43.1074 5.70975L40.4036 8.34L41.4685 9.37305L46 4.97709L41.4685 0.581131L40.4036 1.61418L43.1074 4.24443L4.07973e-07 4.24443L2.91409e-07 5.70975L43.1074 5.70975Z" fill="#323232"/>
                    </svg>
                </a>
            </div>
            <div class="row card-items-2">
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>1</span></div>
                        <p>Услуга локального доступа в Интернет бизнес-класса.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>2</span></div>
                        <p>Включая локальную петлю в зависимости от подходящей локальной инфраструктуры (DSL, оптоволокно).</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>3</span></div>
                        <p>Единое подключение к локальной сети через интерфейс Ethernet со статической общедоступной IP-адресацией.</p>
                    </div>
                </div>
                <div class="col-40">
                    <div class="card card-2">
                        <div class="card-num" data-scroll data-scroll-speed="1"><span>4</span></div>
                        <p>Служба поддержки 24/7/365.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
