<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function globalInternet(){
        $data = [];
        return view('service.global-internet', compact('data'));
    }

    public function cabellInternet(){
        $data = [];
        return view('service.cabell-internet', compact('data'));
    }

    public function ipTranzit(){
        $data = [];
        return view('service.ip-tranzit', compact('data'));
    }

    public function iplc(){
        $data = [];
        return view('service.iplc', compact('data'));
    }

    public function ethernet(){
        $data = [];
        return view('service.ethernet', compact('data'));
    }
}
