<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index(){
        $data = [];
        /*$seo_info = SeoInfo::first();
        $data['seo']['title'] = $seo_info->{'main_page_title_' . App::getLocale()};
        $data['seo']['description'] =$seo_info->{'main_page_description_' . App::getLocale()};
        $data['seo']['keywords'] =  $seo_info->{'main_page_keywords_' . App::getLocale()};*/

        return view('site.index', compact('data'));
    }

    public function aboutUs(){
        $data = [];
        return view('site.about-us', compact('data'));
    }
}
