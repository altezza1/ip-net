<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\front\ServiceController;
use App\Http\Controllers\front\SiteController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale()], function() {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Route::controller(SiteController::class)->group(function () {
        Route::get('/', 'index');
        Route::get('/about-us', 'aboutUs');
    });
    Route::controller(ServiceController::class)->group(function () {
        Route::get('/service/global-internet', 'globalInternet');
        Route::get('/service/cabell-internet', 'cabellInternet');
        Route::get('/service/ip-tranzit', 'ipTranzit');
        Route::get('/service/iplc', 'iplc');
        Route::get('/service/ethernet', 'ethernet');
    });
    Route::controller(ApplicationController::class)->group(function () {
        Route::post('/application/send', 'send');
    });
});
