let body = $('body');
let mobile = false;

if ($(window).width() < 992){
    mobile = true;
}

/*Locomotive with Scrolltrigger*/
gsap.registerPlugin(ScrollTrigger, ScrollToPlugin);

// Using Locomotive Scroll from Locomotive https://github.com/locomotivemtl/locomotive-scroll
const locoScroll = new LocomotiveScroll({
    el: document.querySelector(".smooth-scroll"),
    smooth: true
});
// each time Locomotive Scroll updates, tell ScrollTrigger to update too (sync positioning)
locoScroll.on("scroll", ScrollTrigger.update);

// tell ScrollTrigger to use these proxy methods for the ".smooth-scroll" element since Locomotive Scroll is hijacking things
ScrollTrigger.scrollerProxy(".smooth-scroll", {
    scrollTop(value) {
        return arguments.length ? locoScroll.scrollTo(value, 0, 0) : locoScroll.scroll.instance.scroll.y;
    }, // we don't have to define a scrollLeft because we're only scrolling vertically.
    getBoundingClientRect() {
        return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight};
    },
    // LocomotiveScroll handles things completely differently on mobile devices - it doesn't even transform the container at all! So to get the correct behavior and avoid jitters, we should pin things with position: fixed on mobile. We sense it by checking to see if there's a transform applied to the container (the LocomotiveScroll-controlled element).
    pinType: document.querySelector(".smooth-scroll").style.transform ? "transform" : "fixed"
});

ScrollTrigger.addEventListener("refresh", () => locoScroll.update());
ScrollTrigger.refresh();

/*GSAP ANIMATION*/
/*gsap.timeline({
    scrollTrigger: {
        scroller: (!mobile ? '.smooth-scroll' : ''),
        trigger: "#about-us",
        start: "top top",
        toggleClass: 'test',
        onEnter: () => {
            $('header').addClass('scrolled');
        },
        onLeaveBack: () => {
            $('header').removeClass('scrolled');
        }
    }
})*/

$(function(){
    $(".input-phone").mask("+9(999) 999-99-99");
});

$("img.img-svg").each(function () {
    var $img = $(this);
    var imgClass = $img.attr("class");
    var imgURL = $img.attr("src");
    $.get(imgURL, function (data) {
        var $svg = $(data).find("svg");
        if (typeof imgClass !== "undefined") {
            $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        $svg = $svg.removeAttr("xmlns:a");
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
            $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"))
        }
        $img.replaceWith($svg);
    }, "xml");
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/*Ajax forms*/
$("form.__default").submit(function (e) {
    e.preventDefault();
    let btnSubmit = $(this).find('.btn[type="submit"]')
    btnSubmit.prop('disabled', true);
    $.ajax({
        url: $(this).attr('action'),
        type: "POST",
        data: $(this).serialize(),
        success: function(response) {
            btnSubmit.prop('disabled', false);
            alert(response['success_message'])
            $('.overlay-forms').fadeOut();
        },
        error: function(response) {
            btnSubmit.prop('disabled', false);
            console.log(response);
            alert('Ошибка!');
        }
    });
});

/*menu*/
$('.menu-toggle').click(function () {
    $('.menu .__inner').toggleClass('active')
})

$('.menu a').click(function () {
    $('.menu .__inner').toggleClass('active')
})

$('.menu a.anchor').click(function (e) {
    e.preventDefault()
    let el = $(this).attr('href')
    if ($(el).length){
        locoScroll.scrollTo(el)
    }else{
        window.location.replace('/' + el)
    }
})

var swiper_partners = new Swiper("#swiper_partners", {
    slidesPerView: "4",
    spaceBetween: 70,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints: {
        991: {
            slidesPerView: 2,
            spaceBetween: 60,
        }
    }
});

/*FULL LOAD*/
window.onload = function() {
    locoScroll.update();
    ScrollTrigger.refresh();
    $('html').addClass('loaded');
};
